# Changelog
### v0.0.17
- Fixed build error
### v0.0.16
- Remove Font Awesome library
- Added react-icons
- Updated dependencies to latest version
