import React from "react";
import Col from "react-bootstrap/Col";

export default function Home() {
  return (
    <section id="home">
      <Col md={12}>
        <h1>Home</h1>
      </Col>
      <Col>
        <p>Hey and welcome to my website.</p>
      </Col>
    </section>
  );
}
