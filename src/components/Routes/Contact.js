import React from "react";
import Col from "react-bootstrap/Col";
import Form from "../Contact/Form";

export default function Contact() {
  return (
    <section id="contact">
      <Col lg>
        <h1>Contact</h1>
      </Col>
      <Form />
    </section>
  );
}
