import React from "react";
import Col from "react-bootstrap/Col";

export default function About() {
  return (
    <section id="about">
      <Col>
        <h1>About</h1>
      </Col>
      <Col>
        <p>Here be some about content at some point.</p>
      </Col>
    </section>
  );
}
