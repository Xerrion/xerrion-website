import React from "react";
import ContactInfo from "./ContactInfo";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import SocialLinks from "./SocialLinks";

export default function Footer() {
  return (
    <Row as={"footer"}>
      <Col md={6}>
        <ContactInfo />
      </Col>
      <Col md={6}>
        <SocialLinks />
      </Col>
    </Row>
  );
}
