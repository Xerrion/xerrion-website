import React from "react";
import { FaGitlab, FaTwitter } from "react-icons/fa";

export default function SocialLinks() {
  return (
    <ul id={"social-links"}>
      <li>
        <a
          href={"https://gitlab.com/Xerrion/xerrion-website"}
          target="_blank"
          name="GitLab"
          title="View the source on GitLab"
          rel="noindex nofollow noreferrer noopener"
        >
          <FaGitlab />
        </a>
      </li>
      <li>
        <a
          href={"https://twitter.com/therealxerrion"}
          target="_blank"
          name="Twitter"
          title="Tweet @me on Twitter"
          rel="noindex nofollow noreferrer noopener"
        >
          <FaTwitter />
        </a>
      </li>
    </ul>
  );
}
