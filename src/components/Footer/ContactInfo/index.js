import React from "react";
import { FaPython, FaReact } from "react-icons/fa";

export default function ContactInfo() {
  return (
    <span className="contact-info">
      Made with{" "}
      <span
        style={{ color: "red", background: "none" }}
        role="img"
        aria-label={"Red Heart"}
        title="Love"
      >
        ❤️
      </span>
      ,{" "}
      <a
        href="https://python.org/"
        target="_blank"
        rel="noindex nofollow noreferrer noopener"
        name="Python Software Foundation"
        title="Python 3"
      >
        <FaPython />
      </a>{" "}
      and{" "}
      <a
        href="https://reactjs.org/"
        target="_blank"
        rel="noindex nofollow noreferrer noopener"
        name="React A JavaScript library for building user interfaces"
        title="ReactJS"
      >
        <FaReact />
      </a>{" "}
      by{" "}
      <a
        href="https://xerrion.dk/"
        target="_self"
        name="Lasse Nielsen"
        title="That is me"
      >
        Lasse Nielsen
      </a>
    </span>
  );
}
