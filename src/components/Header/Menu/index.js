import React from "react";
import { Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";


export default function index() {
  return (
    <Navbar collapseOnSelect expand="lg" variant="dark">
      <Navbar.Brand href="#home">
        Xerrion
      </Navbar.Brand>

      <Navbar.Toggle aria-controls="main-navbar" />

      <Navbar.Collapse id="main-navbar" className="justify-content-end">
        <Nav>
          <Nav.Item>
            <Link exact="true" to="/" className="nav-link">
              Home
            </Link>
          </Nav.Item>
          <Nav.Item>
            <Link to="/about" className="nav-link">
              About
            </Link>
          </Nav.Item>
          <Nav.Item>
            <Link to="/portfolio" className="nav-link">
              Portfolio
            </Link>
          </Nav.Item>
          <Nav.Item>
            <Link to="/contact" className="nav-link">
              Contact
            </Link>
          </Nav.Item>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
