import React from "react";
import Menu from "./Menu";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

export default function Header() {
  return (
    <Row as={"header"}>
      <Col as="section" id="menu">
        <Menu />
      </Col>
    </Row>
  );
}
