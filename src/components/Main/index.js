import React from "react";
import Row from "react-bootstrap/Row";
import { Route } from "react-router-dom";
import Home from "../Routes/Home";
import About from "../Routes/About";
import Portfolio from "../Routes/Portfolio";
import Contact from "../Routes/Contact";

export default function Main() {
  return (
    <Row as={"main"} id={"main"}>
      <Route exact path="/" component={Home} />
      <Route path="/about" component={About} />
      <Route path="/portfolio" component={Portfolio} />
      <Route path="/contact" component={Contact} />
    </Row>
    
  );
}
