import React from "react";
import Header from "./Header";
import Main from "./Main";
import Footer from "./Footer";
import Container from "react-bootstrap/Container";
import { BrowserRouter } from "react-router-dom";

function App() {
  return (
    <Container id={"wrapper"}>
      <BrowserRouter>
        <Header />
        <Main />
        <Footer />
      </BrowserRouter>
    </Container>
  );
}

export default App;
